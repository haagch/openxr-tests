// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief Actions Test
 * @author Christoph Haag <christoph.haag@collabora.com>
 */


#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#define hands 2

#include "vulkan/vulkan.h"
#include "vk.h"

#define XR_USE_GRAPHICS_API_VULKAN
#include "openxr/openxr.h"
#include "openxr/openxr_platform.h"

#include "xr-helpers.h"

#define COUNT_OF(x) ((sizeof(x) / sizeof(0 [x])) / ((size_t)(!(sizeof(x) % sizeof(0 [x])))))

//#define RENDER

int
main(int argc, char **argv)
{
	const char *const enabledExtensions[] = {XR_KHR_VULKAN_ENABLE_EXTENSION_NAME};

	XrInstanceCreateInfo instanceCreateInfo = {
	    .type = XR_TYPE_INSTANCE_CREATE_INFO,
	    .enabledExtensionCount = sizeof(enabledExtensions) / sizeof(enabledExtensions[0]),
	    .enabledExtensionNames = enabledExtensions,
	    .applicationInfo =
	        {
	            .applicationName = "Action Test",
	            .engineName = "",
	            .applicationVersion = 1,
	            .engineVersion = 0,
	            .apiVersion = XR_CURRENT_API_VERSION,
	        },
	};

	XrInstance instance;
	CHECK(xrCreateInstance(&instanceCreateInfo, &instance));

	XrSystemGetInfo systemGetInfo = {.type = XR_TYPE_SYSTEM_GET_INFO, .formFactor = XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY};

	XrSystemId systemId;
	CHECK(xrGetSystem(instance, &systemGetInfo, &systemId));

	uint32_t viewConfigurationCount;
	CHECK(xrEnumerateViewConfigurations(instance, systemId, 0, &viewConfigurationCount, NULL));

	XrViewConfigurationType viewConfigurations[viewConfigurationCount];
	CHECK(xrEnumerateViewConfigurations(instance, systemId, viewConfigurationCount, &viewConfigurationCount, viewConfigurations));

	XrViewConfigurationType stereoViewConfigType = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;
	uint32_t view_count;
	CHECK(xrEnumerateViewConfigurationViews(instance, systemId, stereoViewConfigType, 0, &view_count, NULL));
	XrViewConfigurationView *configuration_views = malloc(sizeof(XrViewConfigurationView) * view_count);
	for (uint32_t i = 0; i < view_count; i++) {
		configuration_views[i].type = XR_TYPE_VIEW_CONFIGURATION_VIEW;
		configuration_views[i].next = NULL;
	}

	CHECK(xrEnumerateViewConfigurationViews(instance, systemId, stereoViewConfigType, view_count, &view_count, configuration_views));

	XrGraphicsRequirementsVulkanKHR vk_reqs = {.type = XR_TYPE_GRAPHICS_REQUIREMENTS_VULKAN_KHR};

	PFN_xrGetVulkanGraphicsRequirementsKHR pfnGetVulkanGraphicsRequirementsKHR = NULL;
	CHECK(xrGetInstanceProcAddr(instance, "xrGetVulkanGraphicsRequirementsKHR", (PFN_xrVoidFunction *)&pfnGetVulkanGraphicsRequirementsKHR));
	CHECK(pfnGetVulkanGraphicsRequirementsKHR(instance, systemId, &vk_reqs));

	XrGraphicsBindingVulkanKHR graphics_binding_vk = {.type = XR_TYPE_GRAPHICS_BINDING_VULKAN_KHR};

	initGraphicsBindingVk(&graphics_binding_vk);

	XrSessionCreateInfo session_create_info = {.type = XR_TYPE_SESSION_CREATE_INFO, .next = &graphics_binding_vk, .systemId = systemId};

	XrSession session;
	CHECK(xrCreateSession(instance, &session_create_info, &session));

	XrPosef identityPose = {.orientation = {.x = 0, .y = 0, .z = 0, .w = 1.0}, .position = {.x = 0, .y = 0, .z = 0}};

	XrReferenceSpaceCreateInfo localSpaceCreateInfo = {
	    .type = XR_TYPE_REFERENCE_SPACE_CREATE_INFO, .referenceSpaceType = XR_REFERENCE_SPACE_TYPE_LOCAL, .poseInReferenceSpace = identityPose};

	XrSpace local_space;
	CHECK(xrCreateReferenceSpace(session, &localSpaceCreateInfo, &local_space));

	XrSessionBeginInfo sessionBeginInfo = {.type = XR_TYPE_SESSION_BEGIN_INFO, .primaryViewConfigurationType = stereoViewConfigType};
	CHECK(xrBeginSession(session, &sessionBeginInfo));

	uint32_t swapchainFormatCount;
	CHECK(xrEnumerateSwapchainFormats(session, 0, &swapchainFormatCount, NULL));

	int64_t *swapchainFormats = malloc(sizeof(int64_t) * swapchainFormatCount);
	CHECK(xrEnumerateSwapchainFormats(session, swapchainFormatCount, &swapchainFormatCount, swapchainFormats));

	int64_t swapchainFormatToUse = swapchainFormats[0];

	XrSwapchain *swapchains = malloc(sizeof(XrSwapchain) * view_count);
	uint32_t swapchainLength[view_count];

	XrSwapchainImageVulkanKHR **images = malloc(sizeof(XrSwapchainImageVulkanKHR *) * view_count);
	for (uint32_t i = 0; i < view_count; i++) {
		XrSwapchainCreateInfo swapchainCreateInfo = {
		    .type = XR_TYPE_SWAPCHAIN_CREATE_INFO,
		    .usageFlags = XR_SWAPCHAIN_USAGE_SAMPLED_BIT | XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT,
		    .createFlags = 0,
		    .format = swapchainFormatToUse,
		    .sampleCount = 1,
		    .width = configuration_views[i].recommendedImageRectWidth,
		    .height = configuration_views[i].recommendedImageRectHeight,
		    .faceCount = 1,
		    .arraySize = 1,
		    .mipCount = 1,
		};

		CHECK(xrCreateSwapchain(session, &swapchainCreateInfo, &swapchains[i]));
		CHECK(xrEnumerateSwapchainImages(swapchains[i], 0, &swapchainLength[i], NULL));

		images[i] = malloc(sizeof(XrSwapchainImageVulkanKHR) * swapchainLength[i]);
		for (uint32_t j = 0; j < swapchainLength[i]; j++) {
			images[i][j].type = XR_TYPE_SWAPCHAIN_IMAGE_VULKAN_KHR;
			images[i][j].next = NULL;
		}
		CHECK(xrEnumerateSwapchainImages(swapchains[i], swapchainLength[i], &swapchainLength[i], (XrSwapchainImageBaseHeader *)images[i]));
	}

	XrCompositionLayerProjection projectionLayer = {
	    .type = XR_TYPE_COMPOSITION_LAYER_PROJECTION,
	    .layerFlags = 0,
	    .space = local_space,
	    .viewCount = view_count,
	    .views = NULL,
	};

	XrActionSetCreateInfo exampleSetInfo = {.type = XR_TYPE_ACTION_SET_CREATE_INFO, .priority = 0};
	strcpy(exampleSetInfo.actionSetName, "exampleset");
	strcpy(exampleSetInfo.localizedActionSetName, "Example Action Set");

	XrActionSet exampleSet;
	CHECK(xrCreateActionSet(instance, &exampleSetInfo, &exampleSet));

	XrPath handPaths[hands];
	CHECK(xrStringToPath(instance, "/user/hand/left", &handPaths[0]));
	CHECK(xrStringToPath(instance, "/user/hand/right", &handPaths[1]));

	XrAction grabAction;
	CHECK(create_action(instance, XR_ACTION_TYPE_FLOAT_INPUT, "triggergrab", "Grab Object with Trigger Button", exampleSet, hands, handPaths, &grabAction));
	XrAction leverAction;
	CHECK(create_action(instance, XR_ACTION_TYPE_FLOAT_INPUT, "lever", "Move a lever forward or backward", exampleSet, hands, handPaths, &leverAction));
	XrAction movement2DAction;
	CHECK(create_action(instance, XR_ACTION_TYPE_VECTOR2F_INPUT, "movement2d", "Move in 2D", exampleSet, hands, handPaths, &movement2DAction));
	XrAction poseAction;
	CHECK(create_action(instance, XR_ACTION_TYPE_POSE_INPUT, "handpose", "Hand Pose", exampleSet, hands, handPaths, &poseAction));
	XrAction hapticAction;
	CHECK(create_action(instance, XR_ACTION_TYPE_VIBRATION_OUTPUT, "haptic", "Haptic Vibration", exampleSet, hands, handPaths, &hapticAction));

	struct Binding simple_controller_bindings[] = {
	    {.action = poseAction, .paths = {"/user/hand/left/input/grip/pose", "/user/hand/right/input/grip/pose"}, .path_count = 2},
	    {.action = grabAction, .paths = {"/user/hand/left/input/select/click", "/user/hand/right/input/select/click"}, .path_count = 2},
	    {.action = hapticAction, .paths = {"/user/hand/left/output/haptic", "/user/hand/right/output/haptic"}, .path_count = 2}};
	CHECK(suggest_actions(instance, "/interaction_profiles/khr/simple_controller", simple_controller_bindings, COUNT_OF(simple_controller_bindings)));

	struct Binding index_controller_bindings[] = {
	    {.action = poseAction, .paths = {"/user/hand/left/input/grip/pose", "/user/hand/right/input/grip/pose"}, .path_count = 2},
	    {.action = grabAction, .paths = {"/user/hand/left/input/trigger/value", "/user/hand/right/input/trigger/value"}, .path_count = 2},
	    {.action = leverAction, .paths = {"/user/hand/left/input/thumbstick/y", "/user/hand/right/input/thumbstick/y"}, .path_count = 2},
	    {.action = movement2DAction, .paths = {"/user/hand/left/input/thumbstick", "/user/hand/right/input/thumbstick"}, .path_count = 2},
	    {.action = hapticAction, .paths = {"/user/hand/left/output/haptic", "/user/hand/right/output/haptic"}, .path_count = 2}};
	CHECK(suggest_actions(instance, "/interaction_profiles/valve/index_controller", index_controller_bindings, COUNT_OF(index_controller_bindings)));

	XrActionSpaceCreateInfo actionSpaceInfo = {
	    .type = XR_TYPE_ACTION_SPACE_CREATE_INFO, .action = poseAction, .poseInActionSpace.orientation.w = 1.f, .subactionPath = handPaths[0]};

	XrSpace handSpaces[hands];
	CHECK(xrCreateActionSpace(session, &actionSpaceInfo, &handSpaces[0]));

	actionSpaceInfo.subactionPath = handPaths[1];
	CHECK(xrCreateActionSpace(session, &actionSpaceInfo, &handSpaces[1]));

	XrSessionActionSetsAttachInfo attachInfo = {.type = XR_TYPE_SESSION_ACTION_SETS_ATTACH_INFO, .countActionSets = 1, .actionSets = &exampleSet};
	CHECK(xrAttachSessionActionSets(session, &attachInfo));

	{
		XrBoundSourcesForActionEnumerateInfo sourceInfo = {.type = XR_TYPE_BOUND_SOURCES_FOR_ACTION_ENUMERATE_INFO, .action = leverAction};

		uint32_t numActionSources = 0;
		CHECK(xrEnumerateBoundSourcesForAction(session, &sourceInfo, 0, &numActionSources, NULL));

		printf("lever action has %d sources\n", numActionSources);

		XrPath actionSourcePaths[numActionSources];
		CHECK(xrEnumerateBoundSourcesForAction(session, &sourceInfo, numActionSources, &numActionSources, actionSourcePaths));

		for (uint32_t i = 0; i < numActionSources; i++) {
			XrPath sourcePath = actionSourcePaths[i];
			uint32_t strl = 0;
			CHECK(xrPathToString(instance, sourcePath, 0, &strl, NULL));

			char sourceStr[strl];
			CHECK(xrPathToString(instance, sourcePath, strl, &strl, sourceStr));

			if (sourcePath == XR_NULL_PATH) {
				continue;
			}

			printf("Lever Source path %d: %s\n", i, sourceStr);

			XrInputSourceLocalizedNameGetInfo localizedNameInfo = {
				.type = XR_TYPE_INPUT_SOURCE_LOCALIZED_NAME_GET_INFO, .sourcePath = sourcePath, .whichComponents = XR_INPUT_SOURCE_LOCALIZED_NAME_USER_PATH_BIT | XR_INPUT_SOURCE_LOCALIZED_NAME_INTERACTION_PROFILE_BIT | XR_INPUT_SOURCE_LOCALIZED_NAME_COMPONENT_BIT };

			uint32_t localized_name_len = 0;
			CHECK(xrGetInputSourceLocalizedName(session, &localizedNameInfo, localized_name_len, &localized_name_len, NULL));
			char *localized_name = malloc(sizeof(char) * localized_name_len);
			CHECK(xrGetInputSourceLocalizedName(session, &localizedNameInfo, localized_name_len, &localized_name_len, localized_name));
			printf("Localized source name: %s\n", localized_name);
			free(localized_name);
		}
	}

	XrView *views = malloc(sizeof(XrView) * view_count);
	for (uint32_t i = 0; i < view_count; i++) {
		views[i].type = XR_TYPE_VIEW;
		views[i].next = NULL;
	};

	XrCompositionLayerProjectionView *projection_views = malloc(sizeof(XrCompositionLayerProjectionView) * view_count);
	for (uint32_t i = 0; i < view_count; i++) {
		projection_views[i].type = XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW;
		projection_views[i].next = NULL;
		projection_views[i].fov = views[i].fov;
		projection_views[i].subImage.swapchain = swapchains[i];
		projection_views[i].subImage.imageArrayIndex = 0;
		projection_views[i].subImage.imageRect.offset.x = 0;
		projection_views[i].subImage.imageRect.offset.y = 0;
		projection_views[i].subImage.imageRect.extent.width = configuration_views[i].recommendedImageRectWidth;
		projection_views[i].subImage.imageRect.extent.height = configuration_views[i].recommendedImageRectHeight;
		projectionLayer.views = projection_views;
	}


	XrSessionState state = XR_SESSION_STATE_UNKNOWN;

	while (true) {

		XrEventDataBuffer runtimeEvent = {.type = XR_TYPE_EVENT_DATA_BUFFER, .next = NULL};
		bool should_stop = false;
		XrResult pollResult = xrPollEvent(instance, &runtimeEvent);
		if (pollResult == XR_SUCCESS) {
			switch (runtimeEvent.type) {
			case XR_TYPE_EVENT_DATA_EVENTS_LOST: {
				XrEventDataEventsLost *event = (XrEventDataEventsLost *)&runtimeEvent;
				printf("EVENT: %d events data lost!\n", event->lostEventCount);
				break;
			}
			case XR_TYPE_EVENT_DATA_INSTANCE_LOSS_PENDING: {
				XrEventDataInstanceLossPending *event = (XrEventDataInstanceLossPending *)&runtimeEvent;
				printf("EVENT: instance loss pending at %lu! Destroying instance.\n", event->lossTime);
				break;
			}
			case XR_TYPE_EVENT_DATA_SESSION_STATE_CHANGED: {
				printf("EVENT: session state changed ");
				XrEventDataSessionStateChanged *event = (XrEventDataSessionStateChanged *)&runtimeEvent;
				state = event->state;

				printf("to %d ", state);
				if (event->state == XR_SESSION_STATE_STOPPING || event->state == XR_SESSION_STATE_EXITING ||
				    event->state == XR_SESSION_STATE_LOSS_PENDING) {
					printf("\nSession is in state stopping...");
					should_stop = true;
				}
				printf("\n");
				state = event->state;
				break;
			}
			case XR_TYPE_EVENT_DATA_REFERENCE_SPACE_CHANGE_PENDING: {
				printf("EVENT: reference space change pending!\n");
				XrEventDataReferenceSpaceChangePending *event = (XrEventDataReferenceSpaceChangePending *)&runtimeEvent;
				(void)event;
				// TODO: do something
				break;
			}
			case XR_TYPE_EVENT_DATA_INTERACTION_PROFILE_CHANGED: {
				printf("EVENT: interaction profile changed!\n");
				XrEventDataInteractionProfileChanged *event = (XrEventDataInteractionProfileChanged *)&runtimeEvent;
				(void)event;
				// TODO: do something
				break;
			}

			case XR_TYPE_EVENT_DATA_VISIBILITY_MASK_CHANGED_KHR: {
				printf("EVENT: visibility mask changed!!\n");
				XrEventDataVisibilityMaskChangedKHR *event = (XrEventDataVisibilityMaskChangedKHR *)&runtimeEvent;
				(void)event;
				// this event is from an extension
				break;
			}
			case XR_TYPE_EVENT_DATA_PERF_SETTINGS_EXT: {
				printf("EVENT: perf settings!\n");
				XrEventDataPerfSettingsEXT *event = (XrEventDataPerfSettingsEXT *)&runtimeEvent;
				(void)event;
				// this event is from an extension
				break;
			}
			default: printf("Unhandled event type %d\n", runtimeEvent.type);
			}
		} else if (pollResult == XR_EVENT_UNAVAILABLE) {
			// this is the usual case
		} else {
			printf("Failed to poll events!\n");
			break;
		}

		if (should_stop) {
			printf("Ending session...\n");
			break;
		}

		bool request_exit = false;
		if (request_exit) {
			xrRequestExitSession(session);
		}

		XrFrameState frameState = {.type = XR_TYPE_FRAME_STATE};
		XrFrameWaitInfo frameWaitInfo = {.type = XR_TYPE_FRAME_WAIT_INFO};
		CHECK(xrWaitFrame(session, &frameWaitInfo, &frameState));

		// --- Create projection matrices and view matrices for each eye
		XrViewLocateInfo viewLocateInfo = {.type = XR_TYPE_VIEW_LOCATE_INFO,
		                                   .viewConfigurationType = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO,
		                                   .displayTime = frameState.predictedDisplayTime,
		                                   .space = local_space};

		XrViewState viewState = {.type = XR_TYPE_VIEW_STATE, .next = NULL};
		uint32_t viewCountOutput;
		CHECK(xrLocateViews(session, &viewLocateInfo, &viewState, view_count, &viewCountOutput, views));

		for (uint32_t i = 0; i < view_count; i++) {
			projection_views[i].pose = views[i].pose;
		}

		const XrActiveActionSet activeActionSet = {
		    .actionSet = exampleSet,
		    .subactionPath = XR_NULL_PATH,
		};

		XrActionsSyncInfo syncInfo = {
		    .type = XR_TYPE_ACTIONS_SYNC_INFO,
		    .countActiveActionSets = 1,
		    .activeActionSets = &activeActionSet,
		};

		if (state == XR_SESSION_STATE_FOCUSED) {
			CHECK(xrSyncActions(session, &syncInfo));

			XrActionStateFloat grabValue[hands];
			XrActionStateFloat leverValue[hands];
			XrActionStateVector2f movement2dValue[hands];
			XrSpaceLocation spaceLocation[hands];
			bool spaceLocationValid[hands];

			for (int i = 0; i < hands; i++) {

				XrActionStatePose poseState = {.type = XR_TYPE_ACTION_STATE_POSE, .next = NULL};
				{
					XrActionStateGetInfo getInfo = {
					    .type = XR_TYPE_ACTION_STATE_GET_INFO, .action = poseAction, .subactionPath = handPaths[i]};
					CHECK(xrGetActionStatePose(session, &getInfo, &poseState));
				}
				// printf("Hand pose %d active: %d\n", i, poseState.isActive);

				spaceLocation[i].type = XR_TYPE_SPACE_LOCATION;
				spaceLocation[i].next = NULL;

				CHECK(xrLocateSpace(handSpaces[i], local_space, frameState.predictedDisplayTime, &spaceLocation[i]));

				spaceLocationValid[i] =
				    //(spaceLocation[i].locationFlags & XR_SPACE_LOCATION_POSITION_VALID_BIT) != 0 &&
				    (spaceLocation[i].locationFlags & XR_SPACE_LOCATION_ORIENTATION_VALID_BIT) != 0;

				/*
				printf("Pose %d valid %d: %f %f %f %f, %f %f %f\n", i, spaceLocationValid[i],
				    spaceLocation[0].pose.orientation.x, spaceLocation[0].pose.orientation.y,
				    spaceLocation[0].pose.orientation.z, spaceLocation[0].pose.orientation.w,
				    spaceLocation[0].pose.position.x, spaceLocation[0].pose.position.y,
				    spaceLocation[0].pose.position.z
				);
				*/

				grabValue[i].type = XR_TYPE_ACTION_STATE_FLOAT;
				grabValue[i].next = NULL;
				{
					XrActionStateGetInfo getInfo = {
					    .type = XR_TYPE_ACTION_STATE_GET_INFO, .action = grabAction, .subactionPath = handPaths[i]};

					CHECK(xrGetActionStateFloat(session, &getInfo, &grabValue[i]));
				}

				// printf("Grab %d active %d, current %f, changed %d\n", i, grabValue[i].isActive,
				// grabValue[i].currentState, grabValue[i].changedSinceLastSync);

				if (grabValue[i].isActive && grabValue[i].currentState > 0.75) {
					XrHapticVibration vibration = {.type = XR_TYPE_HAPTIC_VIBRATION,
					                               .amplitude = 0.5,
					                               .duration = XR_MIN_HAPTIC_DURATION,
					                               .frequency = XR_FREQUENCY_UNSPECIFIED};

					XrHapticActionInfo hapticActionInfo = {
					    .type = XR_TYPE_HAPTIC_ACTION_INFO, .action = hapticAction, .subactionPath = handPaths[i]};
					CHECK(xrApplyHapticFeedback(session, &hapticActionInfo, (const XrHapticBaseHeader *)&vibration));
					// printf("Sent haptic output to hand %d\n", i);
				}


				leverValue[i].type = XR_TYPE_ACTION_STATE_FLOAT;
				leverValue[i].next = NULL;
				{
					XrActionStateGetInfo getInfo = {
					    .type = XR_TYPE_ACTION_STATE_GET_INFO, .action = leverAction, .subactionPath = handPaths[i]};

					CHECK(xrGetActionStateFloat(session, &getInfo, &leverValue[i]));
				}
				if (leverValue[i].isActive && leverValue[i].currentState != 0) {
					printf("Lever value %d: changed %d: %f\n", i, leverValue[i].changedSinceLastSync, leverValue[i].currentState);
				}

				movement2dValue[i].type = XR_TYPE_ACTION_STATE_VECTOR2F;
				movement2dValue[i].next = NULL;
				{
					XrActionStateGetInfo getInfo = {
					    .type = XR_TYPE_ACTION_STATE_GET_INFO, .action = movement2DAction, .subactionPath = handPaths[i]};

					CHECK(xrGetActionStateVector2f(session, &getInfo, &movement2dValue[i]));
				}
				// printf("movement action active %d, lever action active %d\n", movement2dValue[i].isActive, leverValue[i].isActive);
				if (movement2dValue[i].isActive && (movement2dValue[i].currentState.x != 0 || movement2dValue[i].currentState.y != 0)) {
					printf("movement value %d: changed %d: %f %f\n", i, movement2dValue[i].changedSinceLastSync,
					       movement2dValue[i].currentState.x, movement2dValue[i].currentState.y);
				}
			};
		}


		XrFrameBeginInfo frameBeginInfo = {.type = XR_TYPE_FRAME_BEGIN_INFO};
		CHECK(xrBeginFrame(session, &frameBeginInfo));

#ifdef RENDER
		for (uint32_t i = 0; i < view_count; i++) {
			// TODO create projection matrix

			XrSwapchainImageAcquireInfo swapchainImageAcquireInfo = {.type = XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO, .next = NULL};
			uint32_t bufferIndex;
			CHECK(xrAcquireSwapchainImage(swapchains[i], &swapchainImageAcquireInfo, &bufferIndex));

			XrSwapchainImageWaitInfo swapchainImageWaitInfo = {.type = XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO, .timeout = 1000};
			CHECK(xrWaitSwapchainImage(swapchains[i], &swapchainImageWaitInfo));

			// TODO render

			XrSwapchainImageReleaseInfo swapchainImageReleaseInfo = {.type = XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO};
			CHECK(xrReleaseSwapchainImage(swapchains[i], &swapchainImageReleaseInfo));
		}
#endif

		const XrCompositionLayerBaseHeader *const submittedLayers[] = {(const XrCompositionLayerBaseHeader *const) & projectionLayer};
		XrFrameEndInfo frameEndInfo = {.type = XR_TYPE_FRAME_END_INFO,
		                               .displayTime = frameState.predictedDisplayTime,
#ifdef RENDER
		                               .layerCount = sizeof(submittedLayers) / sizeof(submittedLayers[0]),
#else
		                               .layerCount = 0,
#endif
		                               .layers = submittedLayers,
		                               .environmentBlendMode = XR_ENVIRONMENT_BLEND_MODE_OPAQUE,
		                               .next = NULL};
		CHECK(xrEndFrame(session, &frameEndInfo));
	}

	xrEndSession(session);
	xrDestroySession(session);
	xrDestroyInstance(instance);
	return 0;
}
