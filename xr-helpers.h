#include "openxr/openxr.h"

#pragma once

#define MAX_BINDING_PATHS 2

#define CHECK(cmd)                                                                                                                                             \
	{                                                                                                                                                      \
		XrResult result = (cmd);                                                                                                                       \
		if (!XR_SUCCEEDED(result)) {                                                                                                                   \
			char resultString[XR_MAX_RESULT_STRING_SIZE];                                                                                          \
			xrResultToString(instance, result, resultString);                                                                                      \
			printf("Error in line %d: [%s]\n", __LINE__, resultString);                                                                            \
			return 1;                                                                                                                              \
		}                                                                                                                                              \
	}

XrResult
create_action(
    XrInstance instance, XrActionType type, char *name, char *localized_name, XrActionSet set, int subaction_count, XrPath *subactions, XrAction *out_action)
{

	XrActionCreateInfo actionInfo = {
	    .type = XR_TYPE_ACTION_CREATE_INFO, .actionType = type, .countSubactionPaths = subaction_count, .subactionPaths = subactions};
	strcpy(actionInfo.actionName, name);
	strcpy(actionInfo.localizedActionName, localized_name);

	return xrCreateAction(set, &actionInfo, out_action);
}

struct Binding
{
	XrAction action;
	char *paths[MAX_BINDING_PATHS];
	int path_count;
};

XrResult
suggest_actions(XrInstance instance, char *profile, struct Binding *b, int binding_count)
{
	XrPath interactionProfilePath;
	CHECK(xrStringToPath(instance, profile, &interactionProfilePath));

	int total = 0;
	for (int i = 0; i < binding_count; i++) {
		struct Binding *binding = &b[i];
		total += binding->path_count;
	}

	XrActionSuggestedBinding *bindings = malloc(sizeof(XrActionSuggestedBinding) * total);

	printf("Suggesting %d actions for %s\n", binding_count, profile);

	int processed_bindings = 0;
	for (int i = 0; i < binding_count; i++) {
		struct Binding *binding = &b[i];

		for (int j = 0; j < binding->path_count; j++) {
			XrPath path;
			CHECK(xrStringToPath(instance, binding->paths[j], &path));

			int current = processed_bindings++;
			bindings[current].action = binding->action;
			bindings[current].binding = path;

			printf("%d: %s\n", j, binding->paths[j]);
		}
	};

	const XrInteractionProfileSuggestedBinding suggestedBindings = {.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING,
	                                                                .interactionProfile = interactionProfilePath,
	                                                                .countSuggestedBindings = total,
	                                                                .suggestedBindings = bindings};

	CHECK(xrSuggestInteractionProfileBindings(instance, &suggestedBindings));

	free(bindings);

	return XR_SUCCESS;
}
