#include "vulkan/vulkan.h"

#pragma once

#define XR_USE_GRAPHICS_API_VULKAN
#include "openxr/openxr.h"
#include "openxr/openxr_platform.h"

#define COUNT_OF(x) ((sizeof(x) / sizeof(0 [x])) / ((size_t)(!(sizeof(x) % sizeof(0 [x])))))

const char *instance_extensions[] = {VK_KHR_EXTERNAL_FENCE_CAPABILITIES_EXTENSION_NAME, VK_KHR_EXTERNAL_MEMORY_CAPABILITIES_EXTENSION_NAME,
                                     VK_KHR_EXTERNAL_SEMAPHORE_CAPABILITIES_EXTENSION_NAME, VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME,
                                     VK_KHR_SURFACE_EXTENSION_NAME};

const char *device_extensions[] = {VK_KHR_SWAPCHAIN_EXTENSION_NAME,
                                   VK_KHR_EXTERNAL_FENCE_EXTENSION_NAME,
                                   VK_KHR_EXTERNAL_FENCE_FD_EXTENSION_NAME,
                                   VK_KHR_EXTERNAL_MEMORY_EXTENSION_NAME,
                                   VK_KHR_EXTERNAL_MEMORY_FD_EXTENSION_NAME,
                                   VK_KHR_EXTERNAL_SEMAPHORE_EXTENSION_NAME,
                                   VK_KHR_EXTERNAL_SEMAPHORE_FD_EXTENSION_NAME,
                                   VK_KHR_GET_MEMORY_REQUIREMENTS_2_EXTENSION_NAME,
                                   VK_KHR_DEDICATED_ALLOCATION_EXTENSION_NAME};

VkResult
initVkInstance(VkInstance *instance)
{
	VkApplicationInfo appInfo = {
	    .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
	    .pApplicationName = "XR Tests",
	    .applicationVersion = VK_MAKE_VERSION(1, 0, 0),
	    .pEngineName = "",
	    .engineVersion = VK_MAKE_VERSION(1, 0, 0),
	    .apiVersion = VK_API_VERSION_1_2,
	};

	VkInstanceCreateInfo createInfo = {.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
	                                   .pApplicationInfo = &appInfo,
	                                   .ppEnabledExtensionNames = instance_extensions,
	                                   .enabledExtensionCount = COUNT_OF(instance_extensions)};

	return vkCreateInstance(&createInfo, NULL, instance);
}

VkResult
initVkDevice(VkInstance instance, VkPhysicalDevice *physicalDevice)
{
	int device_index = 0;
	uint32_t deviceCount = 0;
	vkEnumeratePhysicalDevices(instance, &deviceCount, NULL);
	VkPhysicalDevice *devices = malloc(sizeof(VkPhysicalDevice) * deviceCount);
	vkEnumeratePhysicalDevices(instance, &deviceCount, devices);
	*physicalDevice = devices[device_index];
	// free(devices);
	return VK_SUCCESS;
}

VkResult
getVkGraphicsQueueFamilyIndex(VkPhysicalDevice physicalDevice, uint32_t *index)
{
	uint32_t graphicsQueueFamilyIndex = UINT32_MAX;

	uint32_t queueFamiliesCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamiliesCount, NULL);
	VkQueueFamilyProperties *props = malloc(sizeof(VkQueueFamilyProperties) * queueFamiliesCount);
	vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamiliesCount, props);
	for (uint32_t i = 0; i < queueFamiliesCount; i++) {
		if ((props[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) != 0) {
			graphicsQueueFamilyIndex = i;
			break;
		}
	}
	free(props);

	if (graphicsQueueFamilyIndex == UINT32_MAX)
		return VK_ERROR_FEATURE_NOT_PRESENT;

	*index = graphicsQueueFamilyIndex;
	return VK_SUCCESS;
}

VkResult
initVkLogicalDevice(VkInstance instance, VkPhysicalDevice physicalDevice, uint32_t graphicsQueueFamilyIndex, VkDevice *device)
{
	float queue_priorities[1] = {0.0f};

	VkDeviceQueueCreateInfo deviceQueueInfo = {.flags = 0,
	                                           .queueFamilyIndex = graphicsQueueFamilyIndex,
	                                           .pQueuePriorities = queue_priorities,
	                                           .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
	                                           .queueCount = 1};

	VkPhysicalDeviceFeatures features;
	vkGetPhysicalDeviceFeatures(physicalDevice, &features);
	features.robustBufferAccess = false; // ??

	VkDeviceCreateInfo deviceInfo = {.flags = 0,
	                                 .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
	                                 .enabledLayerCount = 0,
	                                 .queueCreateInfoCount = 1,
	                                 .pQueueCreateInfos = &deviceQueueInfo,
	                                 .pEnabledFeatures = &features,
	                                 .ppEnabledExtensionNames = device_extensions,
	                                 .enabledExtensionCount = COUNT_OF(device_extensions)};

	return vkCreateDevice(physicalDevice, &deviceInfo, NULL, device);
}

VkResult
initGraphicsBindingVk(XrGraphicsBindingVulkanKHR *graphics_binding_vk)
{
	// TODO: can query exts and physical device from runtime
	initVkInstance(&graphics_binding_vk->instance);
	initVkDevice(graphics_binding_vk->instance, &graphics_binding_vk->physicalDevice);
	getVkGraphicsQueueFamilyIndex(graphics_binding_vk->physicalDevice, &graphics_binding_vk->queueFamilyIndex);
	initVkLogicalDevice(graphics_binding_vk->instance, graphics_binding_vk->physicalDevice, graphics_binding_vk->queueFamilyIndex,
	                    &graphics_binding_vk->device);
	return VK_SUCCESS;
}
