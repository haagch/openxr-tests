# OpenXR Tests

## Without rendering

Using dummy vulkan instance, submitting 0 layers:

* openxr-actions-vk: Bind a bunch of actions and query their status in dummy main loop.

## OpenXR runtime

Make sure either a runtime is set as active runtime

    mkdir -p ~/.config/openxr/1
    ln -s ~/monado/build/openxr_monado-dev.json ~/.config/openxr/1/active_runtime.json

or use the `XR_RUNTIME_JSON` environment variable

    XR_RUNTIME_JSON=~/monado/build/openxr_monado-dev.json ./build/openxr-actions-vk
